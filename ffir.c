/*
 * Fast FIR filter based on FFT
 * File: "ffir.c"
 */

//----------------------------------------------------------------------------
#include "ffir.h"
#include <stdio.h> // fprintf(), stderr
//---------------------------------------------------------------------------
// init fast FIR component, allocate memory
// (return 0 or negative error code)
int ffir_init(
  ffir_t *self,      // component structure
  int ns,            // FIR size
  int nf,            // FFT/IFFT size (nf >= ns)
  int nd,            // decimation (>=1)
  const float *taps, // complex FIR taps [ns*2]
  void (*callback)(  // callback
    int nw,            // number of output items (nw = nf - ns + 1)
    const float *out,  // output items [nw*2]
    void *context),    // context
  void *context)     // callback context
{
  int i;
  float corr;

  // check and save parameters
  self->ns = ns > 0 ? ns : 1;
  self->nf = nf >= self->ns ? nf : self->ns;
  self->nw = self->nf - self->ns + 1;
  self->nd = nd > 0 ? nd : 1; 
  
  // amplitude correction
  corr = 1. / (float) self->nf;
  
  // save callback pointers
  self->callback = callback;
  self->context  = context;

  // allocate memory
  self->fft_in = (fftwf_complex*)
                 fftwf_malloc(sizeof(fftwf_complex) * self->nf * 4);
  if (self->fft_in == (fftwf_complex*) NULL)
  {
    fprintf(stderr, "error: fftwf_malloc() return NULL in ffir_init()\n");
    return -1;
  }
  self->fft_out  = self->fft_in   + self->nf;
  self->ifft_out = self->fft_out  + self->nf;
  self->fft_taps = self->ifft_out + self->nf;

  // make FFT plan: "fft_in -> fft_out"
  self->fft_plan = fftwf_plan_dft_1d(self->nf,
                                     self->fft_in, self->fft_out,
                                     FFTW_FORWARD, FFTW_ESTIMATE);
  
  // make IFFT plan: "fft_out -> ifft_out"
  self->ifft_plan = fftwf_plan_dft_1d(self->nf,
                                      self->fft_out, self->ifft_out,
                                      FFTW_BACKWARD, FFTW_ESTIMATE);

  // prepare FFT taps (self->fft_taps)
  for (i = 0; i < self->ns; i++)
  {
    self->fft_in[i][0] = *taps++; // re
    self->fft_in[i][1] = *taps++; // im
  }

  for (i = self->ns; i < self->nf; i++)
  {
    self->fft_in[i][0] = 0.; // re
    self->fft_in[i][1] = 0.; // im
  }

  fftwf_execute(self->fft_plan); // do FFT "fft_in -> fft_out"

  for (i = 0; i < self->nf; i++)
  {
    self->fft_taps[i][0] = self->fft_out[i][0] * corr; // re
    self->fft_taps[i][1] = self->fft_out[i][1] * corr; // re
  }

  // init start of input buffer by zero
  for (i = 0; i < self->ns - 1; i++)
  {
    self->fft_in[i][0] = 0.; // re
    self->fft_in[i][1] = 0.; // im
  }
  
  // init state machine variables
  self->cnt = self->nw;
  self->pin = self->fft_in + (self->ns - 1);
  self->dec_ix = 0;

  return 0;
}
//---------------------------------------------------------------------------
// free memory of fast FIR component
void ffir_free(ffir_t *self)
{
  fftwf_destroy_plan(self->ifft_plan);
  fftwf_destroy_plan(self->fft_plan);
  fftwf_free(self->fft_in);
}
//---------------------------------------------------------------------------
// min funcion (return number of callbacks)
int ffir_do(
  ffir_t *self,    // component structure
  int num,         // number of input items
  const float *in) // input complex items [num*2]
{
  int i, part, cnt = 0;

  while (num > 0)
  {
    i = part = (num < self->cnt) ? num : self->cnt;
    do {
      *self->pin[0] = *in++; // re
      *self->pin[1] = *in++; // im
      self->pin++;
    } while (--i > 0);

    self->cnt -= part;
    if (self->cnt == 0)
    { // run FFT/IFFT
      float *p = (float*) self->fft_out;
      float *q = (float*) self->fft_taps;
  
      // do FFT "fft_in -> fft_out"
      fftwf_execute(self->fft_plan);
    
      // multiply spector to FFT taps
      for (i = 0; i < self->nf; i++)
      {
	float re1 = *p++;
	float im1 = *p--;
	float re2 = *q++;
	float im2 = *q++;
	*p++ = re1 * re2 + im1 * im2;
	*p++ = im1 * re2 - re1 * im2;
      }
      
      // do IFFT "fft_out -> ifft_out"
      fftwf_execute(self->ifft_plan);


      // run callback
      if (1 || self->nd > 1)
      { // decimation
	int step = self->nd * 2 - 1;
	int nout = (self->nw - self->dec_ix + self->nd - 1) / self->nd;
	//int nout = 0;
        
        p = (float*)  self->ifft_out;
        q = (float*) (self->ifft_out + self->dec_ix);
	for (i = self->dec_ix; i < self->nw; i += self->nd)
	{
          *p++ = *q++; // re
	  *p++ = *q;   // im
          q += step;
	  //nout++;
	}
	self->dec_ix = i - self->nw;

        self->callback(nout, (const float*) self->ifft_out, self->context);
      }
      else
      { // no decimation
        self->callback(self->nw, (const float*) self->ifft_out, self->context);
      }

      // move tail to head
      p = (float*)  self->fft_in;
      q = (float*) (self->fft_in + self->nw);
      for (i = self->ns - 1; i > 0; i--)
      {
        *p++ = *q++;
        *p++ = *q++;
      }

      self->cnt = self->nw;
      self->pin = self->fft_in + (self->ns - 1);
      cnt++;
    }

    num -= part;
  }

  return cnt;
}
//---------------------------------------------------------------------------

/*** end of "ffir.c" file ***/

