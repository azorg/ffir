#----------------------------------------------------------------------------
OUT_NAME    := ffir_test
#OUT_DIR    := .
CLEAN_FILES := "$(OUT_DIR)/$(OUT_NAME).exe" a.out
#----------------------------------------------------------------------------
# 1-st way to select source files
SRCS := \
        ffir.c \
        sfir.c \
        ffir_test.c
        
HDRS := ffir.h \
        sfir.h

# 2-nd way to select source files
#SRC_DIRS := .
#HDR_DIRS := .
#----------------------------------------------------------------------------
#DEFS   :=
OPTIM   := -g -O0
#OPTIM  := -Os
WARN    := -Wall -Wno-pointer-to-int-cast
CFLAGS  := $(WARN) $(OPTIM) $(DEFS) $(CFLAGS) -pipe
LDFLAGS := -lm -lfftw3f $(LDFLAGS)
PREFIX  := /usr/local
#----------------------------------------------------------------------------
#_AS  := @as
#_CC  := @gcc
#_CXX := @g++
#_LD  := @gcc

#_CC  := @clang
#_CXX := @clang++
#_LD  := @clang
#----------------------------------------------------------------------------
#DEPS_DIR := .dep
#OBJS_DIR := .obj
#----------------------------------------------------------------------------
include Makefile.skel
#----------------------------------------------------------------------------

