/*
 * Fast FIR filter based on FFT
 * File: "ffir.h"
 */

#ifndef FFIR_H
#define FFIR_H
//---------------------------------------------------------------------------
#include <fftw3.h>
//---------------------------------------------------------------------------
// component structure
typedef struct ffir_ {
  // constans parameters
  int ns; // FIR size
  int nf; // FFT/IFFT size
  int nw; // window size (nw = nf - ns + 1)
  int nd; // decimation (>=1)

  // callback pointers
  void (*callback)( // callback
    int nw,            // number of output items (nw = nf - ns + 1)
    const float *out,  // output items [nw*2]
    void *context);    // context
  void *context;    // callback context

  // FFTW buffers
  fftwf_complex *fft_in;   // input  of FFT
  fftwf_complex *fft_out;  // output of FFT (input of IFFT)
  fftwf_complex *ifft_out; // output of IFFT
  fftwf_complex *fft_taps; // FFT taps
  
  // FFTW3 plans
  fftwf_plan fft_plan;  // FFTW3 direct plan (fft_in -> fft_out)
  fftwf_plan ifft_plan; // FFTW3 inverse plan (fft_out -> ifft_out)

  // state machine variables
  int cnt;            // input counter [0..nw]
  fftwf_complex *pin; // pointer to next input item in fft_in[]
  int dec_ix;         // decimation index
} ffir_t;
//---------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//---------------------------------------------------------------------------
// init fast FIR component, allocate memory
// (return 0 or negative error code)
int ffir_init(
  ffir_t *self,      // component structure
  int ns,            // FIR size
  int nf,            // FFT/IFFT size (nf >= ns)
  int nd,            // decimation (>=1)
  const float *taps, // complex FIR taps [ns*2]
  void (*callback)(  // callback
    int nw,            // number of output items (nw = nf - ns + 1)
    const float *out,  // output items [nw*2]
    void *context),    // context
  void *context);    // callback context
//---------------------------------------------------------------------------
// free memory of fast FIR component
void ffir_free(ffir_t *self);
//---------------------------------------------------------------------------
// min funcion (return number of callbacks)
int ffir_do(
  ffir_t *self,     // component structure
  int num,          // number of input items
  const float *in); // input complex items [num*2]
//---------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//---------------------------------------------------------------------------
#endif // FFIR_H

/*** end of "ffir.h" file ***/

