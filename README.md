Fast FIR filter based on FFT
============================

FFTW3 library used (single precision)

"ffir.h/ffir.c" - main module, look "ffir.h"
"sfir.h/sfir.c" - slow FIR for unit test, look "sfir.h"
"ffir_test.c"   - unit test

