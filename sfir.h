/*
 * Slow FIR filter
 * File: "sfir.h"
 */

#ifndef SFIR_H
#define SFIR_H
//---------------------------------------------------------------------------
// component structure
typedef struct sfir_ {
  // constans parameters
  int ns; // FIR size
  int nb; // buffer size
  int nw; // window size (nw = nb - ns + 1)
  int nd; // decimation (>=1)

  // buffers
  float *buf;  // [nb*2]
  float *taps; // [ns*2]

  // callback pointers
  void (*callback)( // callback
    int num,           // number of output items
    const float *out,  // output items [num*2]
    void *context);    // context
  void *context;    // callback context

  // state machine variables
  int cnt;    // input counter [0..nw]
  float *pin; // pointer to next input item in buf]
  int dec_ix; // decimation index
} sfir_t;
//---------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//---------------------------------------------------------------------------
// init slow FIR component, allocate memory
// (return 0 or negative error code)
int sfir_init(
  sfir_t *self,      // component structure
  int ns,            // FIR size
  int nb,            // buffer size (nb >= ns)
  int nd,            // decimation (>=1)
  const float *taps, // complex FIR taps [ns*2]
  void (*callback)(  // callback
    int num,           // number of output items
    const float *out,  // output items [num*2]
    void *context),    // context
  void *context);    // callback context
//---------------------------------------------------------------------------
// free memory of slow FIR component
void sfir_free(sfir_t *self);
//---------------------------------------------------------------------------
// min funcion (return number of callbacks)
int sfir_do(
  sfir_t *self,     // component structure
  int num,          // number of input items
  const float *in); // input complex items [num*2]
//---------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//---------------------------------------------------------------------------
#endif // SFIR_H

/*** end of "sfir.h" file ***/

