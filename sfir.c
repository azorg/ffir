/*
 * Slow FIR filter
 * File: "sfir.c"
 */

//----------------------------------------------------------------------------
#include "sfir.h"
#include <stdio.h>  // fprintf(), stderr, NULL
#include <stdlib.h> // malloc(), free()
//---------------------------------------------------------------------------
// init slow FIR component, allocate memory
// (return 0 or negative error code)
int sfir_init(
  sfir_t *self,      // component structure
  int ns,            // FIR size
  int nb,            // buffer size (nb >= ns)
  int nd,            // decimation (>=1)
  const float *taps, // complex FIR taps [ns*2]
  void (*callback)(  // callback
    int num,           // number of output items
    const float *out,  // output items [num*2]
    void *context),    // context
  void *context)     // callback context
{
  int i;
  
  // check and save parameters
  self->ns = ns > 0 ? ns : 1;
  self->nb = nb >= self->ns ? nb : self->ns;
  self->nw = self->nb - self->ns + 1;
  self->nd = nd > 0 ? nd : 1; 
  
  // save callback pointers
  self->callback = callback;
  self->context  = context;

  // allocate memory
  self->buf = (float*) malloc(sizeof(float) * 2 * (self->nb + self->ns));
  if (self->buf == (float*) NULL)
  {
    fprintf(stderr, "error: malloc() return NULL in sfir_init()\n");
    return -1;
  }
  self->taps = self->buf + self->nb * 2;
  
  // save taps
  for (i = 0; i < self->ns * 2; i++)
    self->taps[i] = taps[i];
  
  // init start of input buffer by zero
  for (i = 0; i < (self->ns - 1) * 2; i++)
    self->buf[i] = 0.;

  // init state machine variables
  self->cnt = self->nw;
  self->pin = self->buf + (self->ns - 1) * 2;
  self->dec_ix = 0;

  return 0;
}
//---------------------------------------------------------------------------
// free memory of slow FIR component
void sfir_free(sfir_t *self)
{
  free(self->buf);
}
//---------------------------------------------------------------------------
// min funcion (return number of callbacks)
int sfir_do(
  sfir_t *self,     // component structure
  int num,          // number of input items
  const float *in)  // input complex items [num*2]
{
  int i, j, part, cnt = 0;

  while (num > 0)
  {
    i = part = (num < self->cnt) ? num : self->cnt;
    do {
      *self->pin++ = *in++; // re
      *self->pin++ = *in++; // im
    } while (--i > 0);

    self->cnt -= part;
    if (self->cnt == 0)
    { // do FIR
      float *p, *q, *o = self->buf;

      for (i = 0; i < self->nw; i++)
      {
	float re, im;
	re = im = 0.;
	p = o;
	q = self->taps;

	for (j = 0; j < self->ns; j++)
	{
	  float re1 = *p++;
	  float im1 = *p++;
	  float re2 = *q++;
	  float im2 = *q++;
	  re += re1 * re2 + im1 * im2;
	  im += im1 * re2 - re1 * im2;
	}

	*o++ = re;
	*o++ = im;
      }
  
      // run callback
      if (1 || self->nd > 1)
      { // decimation
	int step = self->nd * 2 - 1;
	int nout = (self->nw - self->dec_ix + self->nd - 1) / self->nd;
        //int nout = 0;

        p = self->buf;
        q = self->buf + self->dec_ix * 2;
	for (i = self->dec_ix; i < self->nw; i += self->nd)
	{
          *p++ = *q++; // re
	  *p++ = *q;   // im
          q += step;
	  //nout++;
	}
	self->dec_ix = i - self->nw;

        self->callback(nout, (const float*) self->buf, self->context);
      }
      else
      { // no decimation
        self->callback(self->nw, (const float*) self->buf, self->context);
      }

      // move tail to head
      p = self->buf;
      q = self->buf + self->nw * 2;
      for (i = self->ns - 1; i > 0; i--)
      {
        *p++ = *q++;
        *p++ = *q++;
      }

      self->cnt = self->nw;
      self->pin = self->buf + (self->ns - 1) * 2;
      cnt++;
    }

    num -= part;
  }

  return cnt;
}
//---------------------------------------------------------------------------

/*** end of "sfir.c" file ***/

