/*
 * Fast FIR filter based on FFT
 * File: "ffir_test.c" - test module
 */

//----------------------------------------------------------------------------
#include "ffir.h"   // `ffir_t`
#include "sfir.h"   // `sfir_t`
#include <stdlib.h> // malloc(), free(), exit()
#include <stdio.h>  // printf(), NULL
#include <math.h>   // sqrt()
//----------------------------------------------------------------------------
// input/output data
float *d_in, *d_out1, *d_out2;
//----------------------------------------------------------------------------
// Barker-13 code
float b13[] = { 1., 1., 1., 1., 1., -1., -1., 1., 1., -1., 1., -1., 1. };
//float b13[] = { 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };
//----------------------------------------------------------------------------
// callback
void callback(
  int nw,            // number of output items
  const float *out,  // output items [nw*2]
  void *context)     // context
{
  float **pp = (float**) context;
  float *p = *pp;
  
  while (nw > 0)
  {
    float re = *p++ = *out++; // re
    float im = *p++ = *out++; // im
    float a = sqrt(re * re + im * im);
    //printf("%f %f %f\n", re, im, a);
    a = a;
    nw--;
  }

  *pp = p;
}
//----------------------------------------------------------------------------
int main()
{
  // constants
  int nc = 13;
  int nt = 7;
  int nf = nt * 32;
  int nb = nt * 31;
  int ns = nt * nc;
  int nd = 3;
  int size  = nt * 100;
  int index = nt * 3;

  // viriables
  ffir_t ffir;
  sfir_t sfir;
  float *taps, *p1, *p2;
  int i, j, err;

  d_in = (float*) malloc(sizeof(float) * 2 * size * 3);
  if (d_in == (float*) NULL)
  {
    printf("error: malloc() reurn NULL; exit(-1)\n");
    exit(-1);
  }
  p1 = d_out1 = d_in   + 2 * size;
  p2 = d_out2 = d_out1 + 2 * size;
  
  taps = (float*) malloc(sizeof(float) * 2 * nt * ns);
  if (taps == (float*) NULL)
  {
    printf("error: malloc() reurn NULL; exit(-2)\n");
    exit(-2);
  }

  for (i = 0; i < size * 2; i++)
  {
    d_in[i] = d_out1[i] = d_out2[i] = 0.;
  }

  for (i = 0; i < nc; i++)
  {
    for (j = 0; j < nt; j++)
    {
      d_in[(index + i * nt + j) * 2 + 0] = b13[i]; // re
      d_in[(index + i * nt + j) * 2 + 1] = 0.;     // im
    
      taps[(i * nt + j) * 2 + 0] = b13[i]; // re
      taps[(i * nt + j) * 2 + 1] = 0.;     // im
    }
  }

  i = ffir_init(&ffir, ns, nf, nd, taps, callback, (void*) &p1); 
  if (i != 0)
  {
    printf("error: ffir_init() reurn %i; exit(-3)\n", i);
    exit(-3);
  }
  
  i = sfir_init(&sfir, ns, nb, nd, taps, callback, (void*) &p2); 
  if (i != 0)
  {
    printf("error: sfir_init() reurn %i; exit(-4)\n", i);
    exit(-3);
  }
  
  ffir_do(&ffir, size, d_in);
  sfir_do(&sfir, size, d_in);

  i = p1 - d_out1; 
  j = p2 - d_out2;
  printf("size1=%i size2=%i\n", i, j);

  err = 0;
  if (j > i) j = i;
  for (i = 0; i < j; i++)
  {
    if (fabsf(d_out1[i] - d_out2[i]) > 1e-5)
    {
      printf("ERROR(%i)! %f != %f\n", i, d_out1[i], d_out2[i]);
      err++;
    }
  }
  if (err == 0)
    printf(">>> test [OK]\n");
  else
    printf(">>> test [FAIL]\n");

  sfir_free(&sfir);
  ffir_free(&ffir);
  free(taps);
  free(d_in);

  return 0;
}
//----------------------------------------------------------------------------

/*** end of "ffir_test.c" file ***/

